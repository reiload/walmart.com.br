# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

Install cucumber in eclipse:

Path:
HELP>>Install New Software>>ADD>>

cucumber
http://cucumber.github.com/cucumber-eclipse/update-site

After OK>>Select>>Cucumber Eclipse Plugin>>Next>>Next

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


### How do I get set up? ###

* Summary of set up

* Configuration JAVA 7

change pom.xml to below:

<build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.3</version>
        <configuration>
          <source>1.7</source>
          <target>1.7</target>
        </configuration>
      </plugin>
    </plugins>
</build>

* Configuration JAVA 8

change pom.xml to below:

<build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.3</version>
        <configuration>
          <source>1.8</source>
          <target>1.8</target>
        </configuration>
      </plugin>
    </plugins>
</build>

** After change >> maven clean >> maven install >>> maven test

* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact