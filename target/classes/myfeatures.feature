Feature: Adicionar um produto ao carrinho

  Scenario Outline: Comprar uma TV no WebSite walmart.com.br
    Given Procurar pelo termo  "<Input>" na busca
    And Validar que a busca teve "<Results>"
    And Clicar em um dos "<Product>" e validar
    And Adicionar o Produto ao carrinho
    Then Abrir o carrinho e validar que o "<Product>" foi adicionado com sucesso
    And Fazer o "<Login>" e "<Password>" e valide o "<Name>"

    Examples: 
      | Input | Results | Product      | Login                         | Password  | Name            |
      | TV    | TV      | LF6350 WebOS | reinaldo.rossetti@outlook.com | s2it@test | Reinaldo Mateus |
