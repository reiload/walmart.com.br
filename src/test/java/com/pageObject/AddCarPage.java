package com.pageObject;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import util.functions;


public class AddCarPage extends AbstractPage {
	
	public AddCarPage(WebDriver driver) {
		super(driver);
	}
	
	//vai iniciar os elementos @FindBy
	private final HomePage CarContainer = PageFactory.initElements(driver, HomePage.class);
	
	// delay set on properties file.
	functions conf = new functions();
	String delay_s = conf.getConfig("sleep");
	String delay_w = conf.getConfig("delay_wait");
	int delay_sleep = Integer.parseInt(delay_s);
	int delay_wait = Integer.parseInt(delay_w);
	
	public AddCarPage add_to_cart() throws Throwable {
		
		
		// aqui envio um enter para o botao do carrinho.
		// caso nao apareca o pop-up, ele somente me informa na mensagem.
		
		CarContainer.car_menu.sendKeys(Keys.ENTER);
		Thread.sleep(delay_sleep);
		System.out.println("Clica no botão");
		
		try{ 
			CarContainer.navegaCarrinho.click();	
		}
		catch(Exception e)
		{
		
			System.out.println("***** Pop-up Nao existe! *****");	
		}
		
		// aqui tive um comportamento estranho as vezes a página fica tentando carregar eternamente.
		// deve ser um problema na versao do firefox.
		// no carregamento da pagina voce pode colocar um tempo de espera abaixo.
		// driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		
		return new AddCarPage(driver);
			
	}
	
	public AddCarPage confirm_product_in_cart(String Product) throws Throwable {
	
		int item = 0;
		//functions.findFrames();

		// aqui vai entrar no menu compra
		// adicionei dois locator para nao ter problema.
	
		try{
				CarContainer.topbar_car.click();

			
		}catch (Exception e){
			
				System.out.println("Exception car");
				CarContainer.topbar_link.click();

			}
		
		// vai procurar os elementos adicionados no carrinho e
		// vai retornar o item com a quantidade encontrada.
		
		
		try{
			item = (Integer) functions.CarItens(By.cssSelector(".my-cart-content-item"), Product);

		
		}catch (Exception e){
		
			System.out.println("CarItens");
			item = (Integer) functions.CarItens(By.xpath(".//*[@id='main']/div/section/div/article/ul/li"), Product);

		}		
		
		Assert.assertTrue(item>0);
		return new AddCarPage(driver);
		
	}
	
}
