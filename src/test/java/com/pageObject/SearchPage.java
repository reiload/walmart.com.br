package com.pageObject;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import util.functions;


public class SearchPage extends AbstractPage {

	public SearchPage(WebDriver driver) {
		super(driver);
	}
	
	private final HomePage HomeContainer = PageFactory.initElements(driver, HomePage.class);
	// delay set on properties file.
	functions conf = new functions();
	String delay_s = conf.getConfig("sleep");
	String delay_w = conf.getConfig("delay_wait");
	String pop = conf.getConfig("pop_up");
	int delay_sleep = Integer.parseInt(delay_s);
	int delay_wait = Integer.parseInt(delay_w);
	Boolean pop_up = Boolean.parseBoolean(pop);
	
	public SearchPage search(String query_input) throws Throwable {
		
		//Se o pop-up existir, envia um ESC para sair do mesmo.
		//depois ele envia a consulta desejada e clica no botao pesquisar.
		
		if (pop_up==true){
			System.out.println("Verifica se o pop-up existe");
			functions.WaitTillElementExist(By.xpath("/html/body/div[2]/div/div[1]/div/div[2]/form/div[2]/span[1]/input"),2);	
		}
		
		Thread.sleep(delay_sleep);
		driver.switchTo().defaultContent();
		System.out.println("Faz a busca");
		HomeContainer.search_menu.sendKeys(query_input);
		HomeContainer.search_button.click();
		
		return new SearchPage(driver);
	
	}

	public SearchPage validate_the_search_results(String Results) throws Throwable {
		
		int count = 0;
		
		driver.switchTo().defaultContent();
		//functions.findFrames();
	    
		// Ele procura todos os elementos da pesquisa
		// fiz um for para encontrar o elemento que eu quero, assim como a quantidade 
	    // de resultados que a pesquisa me trouxe.
	    
	    
	    count = functions.AllSearchResults(By.cssSelector("#category-products>section>ul>li>div>a>span"));
		
		System.out.println("Resultados encontrados: " + count + "\n");
		Thread.sleep(delay_sleep);
		
		// retorno para meu steps o resultado da pesquisa
		// caso o valor seja 0, a pesquisa nao trouxe nenhum resultado, sendo assim o teste falhou.
		Assert.assertTrue(count>0);
		
		return new SearchPage(driver);

	}
	
	public SearchPage clicar_e_validar(String Product) throws Exception  {

		int count_p = 0;
		Boolean Test = false;
		Boolean Click;
		String text_result = "";
		String product_title = null;
		String css_itens = "#category-products>section>ul>li>div>a>span";
		
		// traz o numero do elemento selecionado

		count_p = functions.searchElement(By.cssSelector(css_itens), Product);
		int count = count_p;
		
		// neste passo vai encontrar a busca que deseja e vai fazer o click.
		// tirei o caminho absoluto do xpath, pode ser que o caminho absoluto mude e der erro no nosso script.
		// No xpath o primeiro elemento vai se 1, no css o primeiro elemento vai ser 0.
		
		String path_xpath = "//*[@id='category-products']/section/ul/li["+(count+1)+"]/div/a";
		String path_css = "#category-products > section > ul > li.shelf-item.item-"+count+".with-buy-box div.inner-content>a>span";
		System.out.println(path_xpath);
		System.out.println(path_css);
		
		// mudei a ordem como css � mais rapido, fica por primeiro
		// em caso de dar erro com css, ele vai tentar executar via xpath,
		// assim deixamos o codigo mais robusto.
		
		try{
			
			System.out.println("Click Text by css");
			product_title = functions.getText(By.cssSelector(path_css), delay_wait);
			System.out.println("product_title_current: "+ product_title);
			

			Click = functions.searchAndClick(By.cssSelector(path_css), delay_wait);
			System.out.println ("text_result: " + Click);
			
			text_result = functions.getTextPageProduct(HomeContainer.text_result,delay_wait);
			System.out.println ("product_title_page: " + text_result);
			

		}catch (Exception e){
			System.out.println("error em searchAndClick" + e);
			if (text_result==""){
				System.out.println("Try by xpath");
				
				product_title = functions.getText(By.xpath(path_xpath), delay_wait);
				System.out.println("getText: " + product_title);
				
				Click = functions.searchAndClick(By.xpath(path_xpath), delay_wait);
				System.out.println("searchAndClick: " + Click);
				
				text_result = functions.getTextPageProduct(HomeContainer.text_result,delay_wait);
				System.out.println("getTextPageProduct: " + text_result);
			}
		}

		
		if (product_title.contains(text_result)){
			System.out.println("Result OK: " + text_result);
			Test = true;
			
		}else{
			System.out.println("Result Failed - Expected:" + product_title + "\nResult:" + text_result);
	        System.out.print("\n ******* Assert Falhou!!! ********\n");
	        Test = false;
		}
		
		Assert.assertTrue(Test);
		
		return new SearchPage(driver) ;
		
		}
	}

