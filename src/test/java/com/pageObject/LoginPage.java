package com.pageObject;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import util.functions;

public class LoginPage extends AbstractPage {

	public LoginPage(WebDriver driver) {
		super(driver);
	}
	private final HomePage loginContainer = PageFactory.initElements(driver, HomePage.class);

	// delay set on properties file.
	functions conf = new functions();
	String delay_s = conf.getConfig("sleep");
	String delay_w = conf.getConfig("delay_wait");
	int delay_sleep = Integer.parseInt(delay_s);
	int delay_wait = Integer.parseInt(delay_w);

	public LoginPage login(String name) throws Throwable {
	
		driver.switchTo().defaultContent();
		functions.click(loginContainer.login_top,delay_wait);
		Thread.sleep(delay_sleep);
		driver.switchTo().frame("iframeLogin");
		loginContainer.login.sendKeys(name);
		return new LoginPage(driver);
		
	}
	

	public LoginPage password(String senha) throws Throwable {

		loginContainer.password.sendKeys(senha);
		loginContainer.password.sendKeys(Keys.RETURN);
		return new LoginPage(driver);
		
	}
	public LoginPage validate_login(String user_name) throws Throwable {

		driver.switchTo().defaultContent();
		System.out.println("Step login_menu.click");
		final HomePage loginValidate = PageFactory.initElements(driver, HomePage.class);	
		//functions.searchAndClick(By.cssSelector("div.profile a.open-link.topbar-buttons.icon-topbar-link"), delay_wait);
		
		try{
			
			System.out.println("Click menu by css");
			// faz o clique no submenu sua conta.
			functions.moveToElement(By.cssSelector("div.profile a.open-link.topbar-buttons.icon-topbar-link"), delay_wait);			
			Boolean testResult = functions.click(loginValidate.login_menu_css,delay_wait);
			System.out.println("testResult: " + testResult);

		}catch (Exception e){
			System.out.println("error to find user menu");
			System.out.println("error em validate_login try by xpath");
			functions.moveToElement(By.xpath("//header/div[2]/div[3]/a"), delay_wait);
			functions.click(loginValidate.login_menu_xpath,delay_wait);
		}
		
		Thread.sleep(delay_sleep);
		System.out.println("Step getText User");
		String test =  loginContainer.user_text.getText();
		System.out.println("user logado: " +test);
		Assert.assertEquals(user_name, test);
		Thread.sleep(delay_sleep);
		
		return new LoginPage(driver);
		
	}
}
