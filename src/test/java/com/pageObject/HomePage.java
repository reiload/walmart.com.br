package com.pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends AbstractPage {


		public HomePage(WebDriver driver) {
			super(driver);
		}
		
		// **********************************************************************************************************************	
		//SearchPage elements. 
		// **********************************************************************************************************************
		
		@FindBy(how=How.XPATH, using="/html/body/div[2]/div/div[1]/div")
		public WebElement pop_up;
		
		@FindBy(how=How.ID, using="suggestion-search")
		public WebElement search_menu;

		@FindBy(how=How.CSS, using="button.search-icon")
		public WebElement search_button;
		
		@FindBy(how=How.XPATH, using="/html/body/div[1]/div/div[4]/article/div[1]/h1")
		public WebElement text_result;
		
		
		// **********************************************************************************************************************
		// LoginPage elements.
		// **********************************************************************************************************************

		@FindBy(how=How.CSS, using="#topbar-login-link")
		@CacheLookup
		public WebElement login_top;
		
		@FindBy(how=How.XPATH, using="/html/body/div[2]/div/div[1]/div")
		@CacheLookup
		public WebElement login_link;
		
		@FindBy(how=How.ID, using="signinField")
		@CacheLookup
		public WebElement login;
		
		@FindBy(how=How.ID, using="password")
		@CacheLookup
		public WebElement password;
		
		@FindBy(how=How.CSS, using="#dropdown-profile > div > section > div > a:nth-child(1)")
		@CacheLookup
		public WebElement login_menu_css;

		@FindBy(how=How.XPATH, using="//header/div[2]/div[3]/div/div/section/div/a[1]")
		@CacheLookup
		public WebElement login_menu_xpath;
		
		@FindBy(how=How.CSS, using="div.welcome.ng-scope div.ng-scope span.profile-name.ng-binding")
		@CacheLookup
		public WebElement user_text;
		
		
		// **********************************************************************************************************************
		// AddCarPage elements.
		//***********************************************************************************************************************
		
		@FindBy(how=How.XPATH, using="/html/body/div[1]/div/div[4]/article/aside[2]/div[1]/div/section/div[2]/div/div[5]/button")
		@CacheLookup
		public WebElement car_menu;
		
		@FindBy(how=How.XPATH, using=".//*[@id='navegaCarrinho']")
		@CacheLookup
		public  WebElement navegaCarrinho;		
		
		@FindBy(how=How.XPATH, using=".//*[@id='site-topbar']/div[2]/div[1]/a")
		@CacheLookup
		public  WebElement topbar_car;		

		@FindBy(how=How.LINK_TEXT, using="Comprar")
		@CacheLookup
		public  WebElement topbar_link;


		

	}
