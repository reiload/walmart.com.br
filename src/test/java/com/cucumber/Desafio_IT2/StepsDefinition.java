package com.cucumber.Desafio_IT2;

import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.pageObject.AddCarPage;
import com.pageObject.LandingPage;
import com.pageObject.LoginPage;
import com.pageObject.SearchPage;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import util.Browsers;
import util.SeleniumGrid;

public class StepsDefinition {
	

	WebDriver driver = Browsers.setBrowser("Firefox");
	//WebDriver driver = SeleniumGrid.setup("Windows","Firefox","ANY");
	LandingPage landingPage;
	SearchPage searchPage;
	AddCarPage addCarPage;
	LoginPage loginPage;

	@Given("^Procurar pelo termo  \"([^\"]*)\" na busca$")
	public void search_and_click(String query_input) throws Throwable {
		
		//Carrega o WebSite Walmart.
		landingPage = new LandingPage(driver);
		landingPage.navigateToWebSite();
		
		//Faz a busca conforme a feature.
		searchPage = new SearchPage(driver);
		searchPage.search(query_input);
	
	}
	
	@Given("^Validar que a busca teve \"([^\"]*)\"$")
	public void validate_the_search_results(String Results) throws Throwable {
		
		//Valida se trouxe resultado na busca.
		searchPage = new SearchPage(driver);
		int count = searchPage.validate_the_search_results(Results);
		Assert.assertTrue(count>0);
	}
	
	@Given("^Clicar em um dos \"([^\"]*)\" e validar$")
	public void clicar_e_validar(String Product) throws Throwable {
		
		// seleciona um produto e valida a pagina.
		searchPage = new SearchPage(driver);
		Boolean Test = searchPage.clicar_e_validar(Product);
		Assert.assertTrue(Test);
	}

	
	@When("^Adicionar o Produto ao carrinho$")
	public void add_to_cart() throws Throwable {
		
		// adiciona o produto ao carrinho.
		addCarPage = new AddCarPage(driver);
		addCarPage.add_to_cart();
			
	}
	
	@Then("^Abra o carrinho e validar que o \"([^\"]*)\" foi adicionado com sucesso$")
	public void confirm_product_in_cart(String Product) throws Throwable {
		
		//Confirma se o produto selecionado foi adicionado ao carrinho.
		addCarPage = new AddCarPage(driver);
		int item = addCarPage.confirm_product_in_cart(Product);
		Assert.assertTrue(item>0);
	}
	
	
	@Then("^Fazer o \"([^\"]*)\" e \"([^\"]*)\" e valide o \"([^\"]*)\"$")
	public void perform_successfully_and(String name, String password, String user_name) throws Throwable {
		
		// Efetua o login
		loginPage = new LoginPage(driver);
		loginPage.login(name);
		loginPage.password(password);
		loginPage.validate_login(user_name);
		
	}
	
	  @After
	    public void tearDown(Scenario scenario) {
		  
		  	// se o cenario falhar ele vai tirar uma imagem.
            if (scenario.isFailed()) {
	                final byte[] screenshot = ((TakesScreenshot) driver)
	                        .getScreenshotAs(OutputType.BYTES);
	                scenario.embed(screenshot, "image/png");
	            }
 
    		// fecha o browser
    		landingPage = new LandingPage(driver);
    		landingPage.closeDriver();
	  	}
	  
	  
}