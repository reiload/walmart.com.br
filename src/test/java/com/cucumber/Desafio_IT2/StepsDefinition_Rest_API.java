package com.cucumber.Desafio_IT2;

import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import util.Rest_assured;
import util.functions;

public class StepsDefinition_Rest_API {
	
	Rest_assured rest_cliente = new Rest_assured();
	String newsite;
	int update_url;
	int update_email;
	functions conf = new functions();
	
	
	@Given("^Criar um novo site definindo uma \"([^\"]*)\" e um \"([^\"]*)\" para o mesmo$")
	public void criar_um_novo_site_definindo_uma_e_um_para_o_mesmo(String url, String email) throws Throwable {
		
		newsite = Rest_assured.createSite("url=" +url + "&email=" + email);
	
	}

	@Then("^Validar que foi criado com sucesso$")
	public void validar_que_foi_criado_com_sucesso() throws Throwable {

		int site = Rest_assured.postRequest(newsite,"");
		Assert.assertEquals(200, site);
	
	}
	
	@Given("^Fazer um update somente da \"([^\"]*)\" desse site$")
	public void fazer_um_update_somente_da_desse_site(String url) throws Throwable {
		String newsite = conf.getConfig("publicKey");
		update_url = Rest_assured.postRequest(newsite,
				"?url="+url);
	}

	@Given("^Fazer um update somente do \"([^\"]*)\" desse site$")
	public void fazer_um_update_somente_do_desse_site(String email) throws Throwable {
		String newsite = conf.getConfig("publicKey");
		update_email = Rest_assured.postRequest(newsite,
				"?email="+email);
	}

	@Then("^validar que foi feito com sucesso$")
	public void validar_que_foi_feito_com_sucesso() throws Throwable {
		Assert.assertEquals(200, update_url);
		Assert.assertEquals(200, update_email);
	}
	
}
