package util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.pageObject.AbstractPage;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;

public class Browsers extends AbstractPage {

	
	public Browsers(WebDriver driver) {
		super(driver);
	}


	public static WebDriver setBrowser(String Browser){
		
		// Testes feitos no Firefox e Chrome
		
		if(Browser.equalsIgnoreCase("Chrome"))
		{
			ChromeDriverManager.getInstance().setup();
			driver = new ChromeDriver();
			
		} else if (Browser.equalsIgnoreCase("Firefox"))
		{
			driver = new FirefoxDriver();
			
		} 
		/*
		 * Nao testado ainda!
		else if (Browser.equalsIgnoreCase("IE"))
		{
			InternetExplorerDriverManager.getInstance().setup();
			driver = new InternetExplorerDriver();
		}*/
		
		return driver;
	}
	
		
	}