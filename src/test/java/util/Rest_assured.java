package util;
import static com.jayway.restassured.RestAssured.get;

import org.json.JSONArray;
import org.json.JSONException;
import org.openqa.jetty.html.List;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.path.xml.XmlPath;
import com.jayway.restassured.response.Response;

import junit.framework.Assert;
import static com.jayway.restassured.RestAssured.*;
import static com.jayway.restassured.parsing.Parser.JSON;


public class Rest_assured {

	static functions conf = new functions();
	static String mollom_site = null;
	

	public static int postRequest(String url, String parameters) throws Exception {
	
		int result;
		String code = null;
	
		try{
			
			 mollom_site = conf.getConfig("mollom_site");
			 // faz o Post de acordo com os parametros passados.
			 String url_path = mollom_site + "/" + url + parameters;
			 System.out.println("path: " + url_path+"\n");
			 final String xml  = post(url_path).
					 andReturn().asString();
			 
			 // Passa a String para XML
			 XmlPath xmlPath = new XmlPath(xml).setRoot("siteResponse");
			 System.out.println(xmlPath);
			 // Pega os parametros do XML.
			 code = xmlPath.get("code");
			 String publicKey = xmlPath.get("site.publicKey");
			 String privateKey = xmlPath.get("site.privateKey");
			 
			 // Imprime os resultados.
	         System.out.println("\nResquest status: " + code);
	         System.out.println(publicKey);
	         System.out.println(privateKey);
	         
	         // Convert String para Inteiro.
	         result = Integer.parseInt(code);
			
	         return result;
	         
		}catch(Exception e){
	         result = Integer.parseInt(code);
	         return result;
		}
	}
	
	public static String createSite(String parameters) throws Exception {
		
		mollom_site = conf.getConfig("mollom_site");
		// faz o Post de acordo com os parametros passados.

		 final String xml  = post(mollom_site +"?" + parameters).
				 andReturn().asString();
		
		 // Passa a String para XML
		 XmlPath xmlPath = new XmlPath(xml).setRoot("siteResponse");
		 
		 // Pega os parametros do XML.
		 String code = xmlPath.get("code");
		 String publicKey = xmlPath.get("site.publicKey");
		 String privateKey = xmlPath.get("site.privateKey");
		 
		 // Imprime os resultados.
         System.out.println("\nCreate site status: " + code);
         System.out.println(publicKey);
         System.out.println(privateKey);
         
         functions function = new functions();
         function.setPropertiesS(publicKey);
	
         return publicKey;
	}
	
	
	public static int deleteSite(String publicKey) throws Exception {
		
		String code = "404";
		try{
			mollom_site = conf.getConfig("mollom_site");
			// faz o Post de acordo com os parametros passados.
	
			 final String xml  = post(mollom_site + "/" + publicKey + "/?delete").
					 andReturn().asString();
			
			 // Passa a String para XML
			 XmlPath xmlPath = new XmlPath(xml).setRoot("siteResponse");
			 
			 // Pega os parametros do XML.
			 code = xmlPath.get("code");
			 
			 // Imprime os resultados.
	         System.out.println("\nDelete site status: " + code);
	         int result = Integer.parseInt(code);
	         return result;
		}
		catch(Exception e){
			System.out.println("Site ja Nao Exite!");
			int result = Integer.parseInt(code);
			return result;
		}		
}}
