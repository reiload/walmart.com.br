package util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.pageObject.AbstractPage;

public class SeleniumGrid extends AbstractPage {
	
	
	  public SeleniumGrid(WebDriver driver) {
		super(driver);
	}
	 
	  private static final Logger log = Logger.getLogger(SeleniumGrid.class.getName());

	public static WebDriver setup(String platform, String browser, String version) 
	  {
		/* Need configure external drivers.
		 * https://code.google.com/p/selenium/wiki/Grid2
		 * http://www.joecolantonio.com/2013/08/01/selenium-webdriver-fix-for-3-common-ie-errors/
		 * See what process is using port 4444 so you can allow the hub to use the default -> netstat -a
		 */
		
		  DesiredCapabilities caps = new DesiredCapabilities();
		  caps.setVersion(version);
	
		  try {
			  
		  //Platforms
		  if(platform.equalsIgnoreCase("Windows"))
			 caps.setPlatform(org.openqa.selenium.Platform.WINDOWS);
		  
		  if(platform.equalsIgnoreCase("MAC"))
		  	 caps.setPlatform(org.openqa.selenium.Platform.MAC);
		
		  if(platform.equalsIgnoreCase("Andorid"))
				 caps.setPlatform(org.openqa.selenium.Platform.ANDROID);
		  
		  if(platform.equalsIgnoreCase("Linux"))
				 caps.setPlatform(org.openqa.selenium.Platform.LINUX);
		  	
		  //Browsers
		 
		  if(browser.equalsIgnoreCase("IE"))
			
			//System.setProperty("webdriver.ie.driver","C:\\drivers\\IEDriverServer_x64_2.48.0\\IEDriverServer.exe");
			caps = DesiredCapabilities.internetExplorer();  
		  	driver = new RemoteWebDriver(new URL("http://127.0.0.1:5556/wd/hub"), caps);
			  	
		  if(browser.equalsIgnoreCase("Firefox"))
			  caps = DesiredCapabilities.firefox();
		  	driver = new RemoteWebDriver(new URL("http://127.0.0.1:5557/wd/hub"), caps);
		  
		  if(browser.equalsIgnoreCase("Chrome"))
				  caps = DesiredCapabilities.chrome();
		  		driver = new RemoteWebDriver(new URL("http://127.0.0.1:5555/wd/hub"), caps);
			  	  
		  if(browser.equalsIgnoreCase("iPad"))
			  caps = DesiredCapabilities.ipad();
		  
		  if(browser.equalsIgnoreCase("Android"))
			  caps = DesiredCapabilities.android();
		     
		  
		  
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			log.log(Level.SEVERE, "Error doing Remote WebDriver", e);
			e.printStackTrace();
		}
		  
		  return driver;
		  
	  }
}
