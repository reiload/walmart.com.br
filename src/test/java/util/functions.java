package util;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.maven.project.MavenProject;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.pageObject.AbstractPage;


public class functions extends AbstractPage{

	public functions() {
		super(driver);
	}

	
	public static boolean WaitTillElementExist(By by, int seconds)
	{
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		
		try{

			WebElement popup = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
			popup.findElement(by).sendKeys(Keys.ESCAPE);
			
			return true;

		}catch(Exception e){
			
			System.out.println("Pop-up nao Existe!");
			return false;
		
		}
	}



	public static int AllSearchResults(By locator) {
		
		int count = 0;
	    List<WebElement> AllSearchResults=driver.findElements(locator);
		
		for(WebElement eachResult:AllSearchResults)
		   {
			  count++;
			  System.out.println("Item: " + count);	
			  System.out.println("Search found: " + eachResult.getText());
			  
		   }
		
		return count;
		
	}



	public static int searchElement(By locator,String Product) {
		
		List<WebElement> AllSearchResults=driver.findElements(locator);
		int count_p = 0;
		int count = 0;
		
		for(WebElement eachResult:AllSearchResults)
		   {

			  
			  
			  if (eachResult.getText().contains(Product))
						{
				    System.out.println("Search found: " + eachResult.getText());
					System.out.println("Produto id: " + count);
					//String validate_text = String.valueOf(eachResult.getText());
					count_p  = count;
					count++;
				}}
		return count_p;
	}

	public static boolean searchAndClick(By locator, int seconds) {
		
		boolean Product = false;
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		WebElement search = null;

			
			search = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(locator)));
			if (search.isEnabled()){

				//click one element javascript
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", search);
				Product = true;
			};

		return Product;
	}

	public static String getText(By locator, int seconds) {
		
		String product_text = "";
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		WebElement search = null;

			
			search = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(locator)));
			if (search.isEnabled()){
				product_text = search.getText();
		
			};

		return product_text;
	}
	
	public static void findFrames() {
		
		List<WebElement> ele = driver.findElements(By.tagName("iframe"));
	    System.out.println("Number of frames in a page :" + ele.size());
		for(WebElement el : ele){
		      //Returns the Id of a frame.
		        System.out.println("Frame Id :" + el.getAttribute("id"));
		      //Returns the Name of a frame.
		        System.out.println("Frame name: " + el.getAttribute("name"));
			
			}
		
	}



	public static Object CarItens(By locator, String Product) throws InterruptedException {
		
		int count = 0;
		List<WebElement> AllSearchResults=driver.findElements(locator);
		System.out.println(Product);
		Thread.sleep(1000);
		
		int item = 0;
		
		for(WebElement eachResult:AllSearchResults)
		   {
			  System.out.println("Search found: " + eachResult.getText());

			count++;
			  if (eachResult.getText().contains(Product))
				{	
					item = count;
					System.out.println("Item encontrado! " + item);
			 
				}else{
					System.out.println("Item nao bate!");	
				}
			  System.out.println("Itens on car: " + count); 
			
		   }
		return item;
	}
	
	public static boolean click(WebElement locator, Integer seconds) throws InterruptedException{
		
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		
		try{
			
			wait.until(ExpectedConditions.visibilityOf(locator)).click();
			return true;
			
		}catch (Exception e){
			
			
			System.out.println("Elemento nao localizado!"); 
			return false;
		
		}
	}
	
	public static String getTextPageProduct(WebElement locator, Integer seconds) throws InterruptedException{
		String text = "";
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		
		try{
			
			text = wait.until(ExpectedConditions.visibilityOf(locator)).getText();
			return text;
			
		}catch (Exception e){
			
			System.out.println("Elemento Texto nao localizado!"); 
			return text;
		
		}
	}

	public String getConfig(String name)  {
		
		InputStream inputStream = null;
		String value = null;
		try {
			Properties prop = new Properties();
			String propFileName = "conf.properties";
 
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
 
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
 
			// get the property value and print it out
			value = prop.getProperty(name);
 			//System.out.println(value);
			return value;

		} catch (Exception e) {
			
			System.out.println("Exception: " + e);
		
		} 
		
		return value;
		
	}
	
	public void setPropertiesKey(String publicKey ) throws IOException, URISyntaxException {
		
		InputStream inputStream = null;
		Properties prop = new Properties();
		String propFileName = "conf.properties";
 
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			
			if (inputStream != null) {
				prop.load(inputStream);
				inputStream.close();
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}

			prop.setProperty("publicKey", publicKey);
		URL url = getClass().getClassLoader().getResource(propFileName);
		prop.store(new FileOutputStream(new File(url.toURI())), null);

	}
	public void setPropertiesSite(String site ) throws IOException, URISyntaxException {
		
		InputStream inputStream = null;
		Properties prop = new Properties();
		String propFileName = "conf.properties";
 
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			
			if (inputStream != null) {
				prop.load(inputStream);
				inputStream.close();
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}

			prop.setProperty("mollom_site", site);
		URL url = getClass().getClassLoader().getResource(propFileName);
		prop.store(new FileOutputStream(new File(url.toURI())), null);

	}
	public static String alertConfirm() {


		String alertText = "";
		try {
			Alert alert = driver.switchTo().alert();
			alertText = alert.getText();
			alert.dismiss();
			// or accept as below
			//alert.accept();

		} catch (NoAlertPresentException nape) {
			// nothing to do, because we only want to close it when pop up
			System.out.println("Alerta nao encontrado!");

		}
		return alertText;
		
	}
	
	public static String moveToElement(By by, Integer seconds) {


		String alertText = "";
		try {
			   WebElement rootMenu = driver.findElement((By) by);

			    Actions action = new Actions(driver);

			    action.moveToElement(rootMenu).perform();

		} catch (NoAlertPresentException nape) {
			// nothing to do, because we only want to close it when pop up
			System.out.println("Error moveToElement!");

		}
		return alertText;
		
	}
	
}
