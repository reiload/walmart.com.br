Feature: Dado uma API p�blica -> Mollom API e sua documenta��o para validacao

  Scenario Outline: Criar um novo site
    Given Criar um novo site definindo uma "<URL>" e um "<Email>" para o mesmo
    Then Validar que foi criado com sucesso

    Examples: 
      | URL                    | Email             |
      | www.vidadetestador.com | reiload@gmail.com |

  Scenario Outline: Fazer update da url e do email no new site
    Given Fazer um update somente da "<URL_Update>" desse site
    And Fazer um update somente do "<Email_Update>" desse site
    Then validar que foi feito com sucesso

    Examples: 
      | URL_Update               | Email_Update                  |
      | www.vidadetestadores.com | reinaldo.rossetti@outlook.com |
