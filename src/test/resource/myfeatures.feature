Feature: Adicionar um produto ao carrinho

  Scenario Outline: Comprar uma TV no WebSite walmart.com.br
    Given Procurar pelo termo  "<Input>" na busca
    And Validar que a busca teve "<Results>"
    And Clicar em um dos "<Product>" e validar
    When Adicionar o Produto ao carrinho
    Then Abra o carrinho e validar que o "<Product>" foi adicionado com sucesso
    And Fazer o "<Login>" e "<Password>" e valide o "<Name>"

    Examples: 
      | Input | Results | Product           | Login                         | Password  | Name            |
      | TV    | TV      | Samsung UN40J6400 | reinaldo.rossetti@outlook.com | xxxx | Reinaldo Mateus |

